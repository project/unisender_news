<?php
namespace Drupal\unisender_news\Unisender;

use Unisender\ApiWrapper;

class UnisenderHelper {
  private $apiKey;
  private $uni;

  public function __construct(){
    $this->apiKey = $this->getApiKey();
    $this->uni = new UnisenderApi($this->apiKey);
  }

  public function getListsOptions(){
    $lists = $this->getLists();
    if(empty($lists)){
      return [];
    }
    $list_options = [];
    foreach($lists->result as $item){
      $list_options[$item->id] = $item->title;
    }
    return $list_options;
  }

  public function getLists(){
    $lists_raw = $this->uni->getLists();
    return json_decode($lists_raw);
  }
  public function getApiKey(){
    $config = \Drupal::config('unisender_news.settings');
    return $config->get('api_key');
  }

  public function callMethod($methodName, $params){
    return $this->uni->{$methodName}($params);
  }
}
