<?php
/**
 * @file
 * Contain unisender message entities.
 */

namespace Drupal\unisender_news\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * @ContentEntityType(
 *   id = "unisender_messages",
 *   label = @Translation("Unisender messages"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\unisender_news\Entity\Controller\UnisenderMessagesEntityBuilder",
 *     "form" = {
 *       "default" = "Drupal\unisender_news\Form\UnisenderMessagesForm",
 *       "delete" = "Drupal\unisender_news\Form\UnisenderMessagesDeleteForm",
 *     },
 *     "access" = "Drupal\unisender_news\UnisenderMessagesControlHandler",
 *   },
 *   base_table = "unisender_messages",
 *   admin_permission = "unisender news admin",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "subject",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/content/unisender-news/{unisender_messages}",
 *     "edit-form" = "/admin/content/unisender-news/{unisender_messages}/edit",
 *     "delete-form" = "/admin/content/unisender-news/{unisender_messages}/delete",
 *     "collection" = "/admin/content/unisender-news"
 *   },
 * )
 */
class UnisenderMessages extends ContentEntityBase implements ContentEntityInterface {

  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the message.'))
      ->setReadOnly(TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the UnisenderMessage entity.'))
      ->setReadOnly(TRUE);

    $fields['message_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Message ID'))
      ->setDescription(t('The ID of the message from unisender.'))
      ->setReadOnly(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that message was created.'));

    $fields['subject'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Message subject'))
      ->setDescription(t('Message subject for newsletter.'))
      ->setSettings([
        'default_value' => '',
        'max_length' => 256,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
        'settings' => [
          'placeholder' => t('Message subject'),
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['message'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Message'))
      ->setDescription(t('Message for current newsletter.'))
      ->setSettings([
        'label' => 'visible',
        'type' => 'text_default',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 6,
        'rows' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['list'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Subscribers list'))
      ->setDescription(t('Subscribers list.'))
      ->setSettings([
        'default_value' => 4,
        'max_length' => 256,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
