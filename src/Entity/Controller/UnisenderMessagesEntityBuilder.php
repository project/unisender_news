<?php

namespace Drupal\unisender_news\Entity\Controller;

use Drupal\unisender_news\Unisender\UnisenderHelper;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * @ingroup unisender_news
 */
class UnisenderMessagesEntityBuilder extends EntityListBuilder {

  /**
   * The url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface
   */
  protected $urlGenerator;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('url_generator')
    );
  }

  /**
   * Constructs a new UnisenderMessages object.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, UrlGeneratorInterface $url_generator) {
    parent::__construct($entity_type, $storage);
    $this->urlGenerator = $url_generator;
    $this->uniHelper = new UnisenderHelper();
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['table'] = parent::render();
    $build['table']['table']['#attributes']['class'] = ['unisender-news-table'];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'date' => $this->t('Date'),
      'label' => $this->t('Title'),
    ];
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row = [];
    $row['date'] = \Drupal::service('date.formatter')
      ->format($entity->get('created')->value, 'custom', 'd.m.Y');
    $linkedLabel = [
      '#markup' => '<a href="/admin/content/unisender-news/' . $entity->get('id')->value . '/edit">' . $entity->get('subject')->value . '</a>',
    ];
    $row['label'] = \Drupal::service('renderer')->render($linkedLabel);
    return $row;
  }
}
