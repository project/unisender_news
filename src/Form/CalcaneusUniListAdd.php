<?php

namespace Drupal\unisender_news\Form;

use Drupal\unisender_news\Unisender\UnisenderHelper;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

class UnisenderNewsListAdd extends FormBase {

  public function getFormId() {
    return 'unisender_news_list_add';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('List title'),
      '#required' => TRUE,
    ];
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $fields = $entityFieldManager->getFieldDefinitions('user', 'user');
    $options = [];
    foreach ($fields as $field) {
      if ($field->getType() == 'boolean' && !in_array($field->getName(), [
          'status',
          'default_langcode',
        ])) {
        $options[$field->getName()] = $field->getLabel();
      }
    }
    $form['condition'] = [
      '#title' => $this->t('Add user if checked field.'),
      '#type' => 'checkboxes',
      '#options' => $options,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add list'),
    ];
    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $UnisenderHelper = new UnisenderHelper();
    $title = $form_state->getValue('title');
    $list = $UnisenderHelper->callMethod('createList', ['title' => $title]);
    $list = json_decode($list);
    $batch = [
      'title' => $this->t('Add users to list'),
      'operations' => [],
    ];

    $conditions = $form_state->getValue('condition');
    $query = \Drupal::entityQuery('user')
      ->condition('status', 1)
      ->condition('uid', 0, '<>');
    foreach ($conditions as $field_name => $condition) {
      if ($condition) {
        $query->condition($field_name, 1);
      }
    }
    $result = $query->execute();
    if ($result) {
      foreach ($result as $uid) {
        $batch['operations'][] = [
          '\Drupal\unisender_news\Form\UnisenderNewsListAdd::processItem',
          [$uid, $list],
        ];
      }
    }
    batch_set($batch);
  }

  public function processItem($uid, $list, array &$context) {
    $user = User::load($uid);
    $uniHelper = new UnisenderHelper();
    $uniHelper->callMethod('subscribe', [
      'list_ids' => $list->result->id,
      'double_optin' => 3,
      'fields[email]' => $user->getEmail(),
      'fields[Name]' => $user->getAccountName(),
    ]);
  }

  public function finished() {

  }

}
