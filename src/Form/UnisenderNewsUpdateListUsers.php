<?php

namespace Drupal\unisender_news\Form;

use Drupal\unisender_news\Unisender\UnisenderHelper;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class UnisenderNewsUpdateListUsers extends FormBase {

  public function getFormId() {
    return 'unisender_news_list_update';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $uniHelper = new UnisenderHelper();
    $list_options = $uniHelper->getListsOptions();
    $form['list'] = [
      '#title' => $this->t('Select list'),
      '#type' => 'select',
      '#options' => $list_options,
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
    ];

    return $form;
  }

   public function submitForm(array &$form, FormStateInterface $form_state) {}
}
