<?php

namespace Drupal\unisender_news\Form;

use Drupal\unisender_news\Unisender\UnisenderHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class UnisenderNewsList extends FormBase {

  public function getFormId() {
    return 'unisender_news_list_page';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $UnisenderHelper = new UnisenderHelper();
    $lists = $UnisenderHelper->getLists();
    $form['unisender_news_lists'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    foreach($lists->result as $list){
      $form['unisender_news_lists'][$list->id]['title'] = [
        '#type' => 'item',
        '#markup' => $list->title,
      ];
      $form['unisender_news_lists'][$list->id]['delete'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Delete list'),
      ];
    }
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete selected lists')
    ];
    $form['#theme'] = 'unisender_news_list';

    return $form;
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->cleanValues();
    $values = $form_state->getValues();
    $UnisenderHelper = new UnisenderHelper();

    foreach($values['unisender_news_lists'] as $id => $list){
      if($list['delete']){
        $UnisenderHelper->callMethod('deleteList', ['list_id' => $id]);
      }
    }
  }

}
