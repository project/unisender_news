<?php

namespace Drupal\unisender_news\Form;

use Drupal\unisender_news\Unisender\Unisender;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class UnisenderNewsAdmin extends ConfigFormBase {

  public function getFormId() {
    return 'unisender_news_admin_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('unisender_news.settings');
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('api_key'),
    ];

    $form['sender_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sender name'),
      '#default_value' => $config->get('sender_name'),
    ];

    $form['sender_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sender email'),
      '#default_value' => $config->get('sender_email'),
    ];

    $form['subscribed_list_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Subscribed list id'),
      '#default_value' => $config->get('subscribed_list_id'),
    ];


    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('unisender_news.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('sender_name', $form_state->getValue('sender_name'))
      ->set('sender_email', $form_state->getValue('sender_email'))
      ->set('subscribed_list_id', $form_state->getValue('subscribed_list_id'))
      ->save();
  }

  protected function getEditableConfigNames() {
    return [
      'unisender_news.settings',
    ];
  }

}
