<?php

namespace Drupal\unisender_news\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\unisender_news\Unisender\UnisenderHelper;


class UnisenderMessagesForm extends ContentEntityForm {


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $uniHelper = new UnisenderHelper();
    $list_options = $uniHelper->getListsOptions();
    $form['list']['widget'][0]['value']['#type'] = 'select';
    $form['list']['widget'][0]['value']['#options'] = $list_options;
    $form['list']['widget'][0]['value']['#required'] = TRUE;
    $form['list']['widget'][0]['value']['#default_value'] = key($form['category']['widget'][0]['value']['#options']);
    $form['message']['widget'][0]['#format'] = "full_html";
    $form['actions']['submit']['#value'] = t("Send");
    return $form;
  }

  /**
   * Submit message form. Called for new message creation page.
   * This callback will be create new message entity and planing newsletter
   * for selected users group.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $entity = $this->entity;
    if ($entity->isNew()) {
      $message = $entity->get('message')->value;
      $list = $entity->get('list')->value;
      $subject = $entity->get('subject')->value;
      $uniHelper = new UnisenderHelper();
      $config = \Drupal::config('unisender_news.settings');
      $params = [
        'sender_name' => $config->get('sender_name'),
        'sender_email' => $config->get('sender_email'),
        'subject' => $subject,
        'body' => $message,
        'list_id' => $list,
        'generate_text' => 1,
      ];
      $message_raw = $uniHelper->callMethod('createEmailMessage', $params);
      $message = json_decode($message_raw);
      if (isset($message->result)) {
        $message_id = $message->result->message_id;
        // Create campaign with track links and track reads.
        // @see https://www.unisender.com/ru/support/api/messages/createcampaign/
        $campaignOptions = [
          'track_read' => 1,
          'track_links' => 1,
          'message_id' => $message_id,
        ];
        $result = $uniHelper->callMethod('createCampaign', $campaignOptions);
        $result = json_decode($result);
        $entity->set('message_id', $result->result->campaign_id);
        $entity->save();
      }
    }
    $form_state->setRedirect('entity.unisender_messages.collection');
  }

}
